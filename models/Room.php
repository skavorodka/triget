<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "rooms".
 *
 * @property int $id
 * @property string $name
 * @property string $image
 * @property string $description
 * @property int $created_at
 * @property int $updated_at
 */
class Room extends \yii\db\ActiveRecord
{
    public $image_field;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rooms';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['name', 'image'], 'string', 'max' => 255],
            ['image_field', 'file', 'extensions' => ['png', 'jpg', 'gif'], 'maxSize' => 1024 * 1024 * 5],
        ];
    }

    /**
     * Relations
     */
    public function getBookings()
    {
        return $this->hasMany(Booking::className(), ['room_id' => 'id'])->orderBy('id DESC');
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'image' => 'Изображение',
            'image_field' => 'Изображение',
            'description' => 'Краткое описание',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ];
    }
}
