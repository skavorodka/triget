<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "bookings".
 *
 * @property int $id
 * @property int $room_id
 * @property string $name
 * @property string $phone
 * @property string $date
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class Booking extends \yii\db\ActiveRecord
{
    const STATUS_NEW        = 1;
    const STATUS_APPROVED   = 2;
    const STATUS_REJECTED   = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bookings';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['room_id', 'name', 'phone', 'date'], 'required'],
            [['room_id', 'status'], 'integer'],
            [['date'], 'safe'],
            [['name', 'phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * Relations
     */
    public function getRoom()
    {
        return $this->hasOne(Room::className(), ['id' => 'room_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'room_id' => 'Номер',
            'name' => 'Имя клиента',
            'phone' => 'Телефон',
            'date' => 'Дата',
            'status' => 'Статус',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ];
    }

    /**
    * Statuses
    */
    public function getStatusName() {
        switch ($this->status) {
            case self::STATUS_NEW:
                return 'Новое';
                break;
            case self::STATUS_APPROVED:
                return 'Одобрено';
                break;
            case self::STATUS_REJECTED:
                return 'Отклонено';
                break;
            default:
                return '-';
                break;
        }
    }

    public function getStatusArr() {
        return [
            self::STATUS_NEW => 'Новое',
            self::STATUS_APPROVED => 'Одобрено',
            self::STATUS_REJECTED => 'Отклонено',
        ];
    }

    public function getStatusLabel() {
        switch ($this->status) {
            case self::STATUS_NEW:
                return '<span class="label label-primary">Новое</span>';
                break;
            case self::STATUS_APPROVED:
                return '<span class="label label-success">Одобрено</span>';
                break;
            case self::STATUS_REJECTED:
                return '<span class="label label-danger">Отклонено</span>';
                break;
            default:
                return '<span class="label label-default">Неизвестен</span>';
                break;
        }
    }
}
