<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Booking;
use yii\behaviors\TimestampBehavior;

/**
 * @property int $room_id
 * @property string $name
 * @property string $phone
 * @property string $date
 */
class BookingForm extends Model
{
    public $room_id;
    public $name;
    public $phone;
    public $date;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['room_id', 'name', 'phone', 'date'], 'required'],
            ['room_id', 'integer'],
            ['date', 'safe'],
            //['date', 'exist', 'targetClass' => Booking::class],
            ['date', 'checkDate'],
            ['name', 'string', 'min' => 2, 'max' => 255],
            //['name', 'match', 'pattern' => '/^([- A-Za-zа-яА-ЯёЁ]+)$/'], // Не работает с русским языком
            ['name', 'checkName'],
            ['phone', 'string', 'max' => 255],
            ['phone', 'match', 'pattern' => '/^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/'],
            // Съедает следующие телефоны: 
            // +7(903)888-88-88
            // 8(999)99-999-99
            // +380(67)777-7-777
            // 001-541-754-3010
            // +1-541-754-3010
            // 19-49-89-636-48018
            // +233 205599853
        ];
    }

    public function checkName($attribute, $params, $validator)
    {
        if (!mb_ereg_match('^([- A-Za-zа-яА-ЯёЁ]+)$', $this->$attribute)) {
            $validator->addError($this, $attribute, 'Значение «{attribute}» неверно.');
        }
    }

    public function checkDate($attribute, $params)
    {
        if (Booking::find()->where(['room_id' => $this->room_id, 'date' => date('Y-m-d', strtotime($this->date))])->exists()) {
            $this->addError($attribute, 'Данное время уже занято для этого номера. Выберите другой номер или время.');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'room_id' => 'Номер',
            'name' => 'Имя клиента',
            'phone' => 'Телефон',
            'date' => 'Дата',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function save()
    {
        $model = new Booking();
        $model->attributes = $this->attributes;
        $model->status = Booking::STATUS_NEW;
        return $model->save();
    }

}