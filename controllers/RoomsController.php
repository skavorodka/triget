<?php

namespace app\controllers;

use Yii;
use app\models\Room;
use app\models\RoomsSearch;
use app\models\BookingForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * RoomsController implements the CRUD actions for Room model.
 */
class RoomsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [

        ];
    }

    /**
     * Lists all Room models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RoomsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $booking_form = new BookingForm();
        if (Yii::$app->request->isAjax && $booking_form->load($_POST)) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($booking_form);
        }
        if ($booking_form->load(Yii::$app->request->post()) && $booking_form->save()) {
            Yii::$app->session->setFlash('success', 'Бронирование прошло успешно');
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'booking_form' => $booking_form,
        ]);
    }

    /**
     * Displays a single Room model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        
        $booking_form = new BookingForm();
        if (Yii::$app->request->isAjax && $booking_form->load($_POST)) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($booking_form);
        }
        if ($booking_form->load(Yii::$app->request->post()) && $booking_form->validate() && $booking_form->save()) {
            Yii::$app->session->setFlash('success', 'Бронирование прошло успешно');
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->render('view', [
            'model' => $model,
            'booking_form' => $booking_form,
        ]);
    }

    /**
     * Finds the Room model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Room the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Room::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
