<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Room */
/* @var $booking_form app\models\BookingForm */
?>

<?php 
	Modal::begin([
	    'header' => '<h2>Забронировать «'.$model->name.'»</h2>',
	    'toggleButton' => ['label' => 'Забронировать', 'class' => 'btn btn-primary'],
	]);
	$booking_form->room_id = $model->id;
?>
	<?php $form = ActiveForm::begin(['enableAjaxValidation' => true]); ?>
    <?= $form->field($booking_form, 'room_id')->hiddenInput()->label(false) ?>
    <?= $form->field($booking_form, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($booking_form, 'phone')->textInput(['maxlength' => true]) ?>
    <?= $form->field($booking_form, 'date')->input('date') ?>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
<?php Modal::end(); ?>