<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use himiklab\thumbnail\EasyThumbnailImage;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RoomsSearch */
/* @var $booking_form app\models\BookingForm */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Номера';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="room-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'room-item'],
        'itemView' => '_item',
        'viewParams' => ['booking_form' => $booking_form],
    ]); ?>
</div>