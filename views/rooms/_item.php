<?php

use yii\helpers\Html;
use himiklab\thumbnail\EasyThumbnailImage;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Room */
/* @var $booking_form app\models\BookingForm */
?>

<h3><?= Html::a($model->name, ['rooms/view', 'id' => $model->id]) ?></h3>
<?= $model->image ? EasyThumbnailImage::thumbnailImg(Yii::getAlias('@webroot').$model->image, 200, 200) : 'Здесь может быть заглушка' ?>
<?= $this->render('_modal', ['model' => $model, 'booking_form' => $booking_form]) ?>