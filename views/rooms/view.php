<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use himiklab\thumbnail\EasyThumbnailImage;

/* @var $this yii\web\View */
/* @var $model app\models\Room */
/* @var $booking_form app\models\BookingForm */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Номера', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

//echo preg_match('/^([- A-Za-zа-яА-ЯёЁ]+)$/', 'Никита');
//echo mb_ereg_match('^([- A-Za-zа-яА-ЯёЁ]+)$', 'Никита') ? 'yes' : 'no';
?>

<div class="room-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $model->image ? EasyThumbnailImage::thumbnailImg(Yii::getAlias('@webroot').$model->image, 400, 400) : 'Здесь может быть заглушка' ?>
    <pre><?= $model->description ?></pre>
    <p>
        <?= $this->render('_modal', ['model' => $model, 'booking_form' => $booking_form]) ?>
    </p>
</div>