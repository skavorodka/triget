<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use himiklab\thumbnail\EasyThumbnailImage;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;

/* @var $this yii\web\View */
/* @var $model app\models\Room */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Номера', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="room-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => $model->image ? EasyThumbnailImage::thumbnailImg(Yii::getAlias('@webroot').$model->image, 400, 400) : '-',
            ],
            'description:ntext',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>
    <h3>Бронирование</h3>
    <?= GridView::widget([
        'dataProvider' => new ArrayDataProvider([
            'allModels' => $model->bookings,
        ]),
        'columns' => [
            'id',
            'name',
            'phone',
            'date:date',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($data) {
                    return $data->statusLabel;
                },
            ],
            //'created_at:datetime',
            //'updated_at:datetime',
            [
                'class' => 'yii\grid\ActionColumn', 'template' => '{approve}{reject}', 'controller' => 'bookings',
                'buttons' => [
                    'approve' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-ok"></span>', ['bookings/approve', 'id' => $model->id], ['title' => 'Одобрить', 'class' => 'btn btn-xs btn-success']);
                    },
                    'reject' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-remove"></span>', ['bookings/reject', 'id' => $model->id], ['title' => 'Отклонить', 'class' => 'btn btn-xs btn-danger', 'data-confirm' => 'Вы уверены?']);
                    },
                ],
            ],
        ],
    ]); ?>
</div>