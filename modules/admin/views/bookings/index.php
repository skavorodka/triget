<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Room;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BookingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Бронирование';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="booking-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'room_id',
                'format' => 'raw',
                'value' => function($data) {
                    return $data->room ? Html::a($data->room->name, ['rooms/view', 'id' => $data->room_id]) : '-';
                },
                'filter' => Html::activeDropDownList($searchModel, 'room_id', ArrayHelper::map(Room::find()->all(), 'id', 'name'), ['class' => 'form-control', 'prompt' => '']),
            ],
            'name',
            'phone',
            'date:date',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($data) {
                    return $data->statusLabel;
                },
                'filter' => Html::activeDropDownList($searchModel, 'status', $searchModel->statusArr, ['class'=>'form-control', 'prompt' => 'Не важно']),
            ],
            //'created_at:datetime',
            //'updated_at:datetime',
            [
                'class' => 'yii\grid\ActionColumn', 'template' => '{approve}{reject}',
                'buttons' => [
                    'approve' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-ok"></span>', $url, ['title' => 'Одобрить', 'class' => 'btn btn-xs btn-success']);
                    },
                    'reject' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, ['title' => 'Отклонить', 'class' => 'btn btn-xs btn-danger', 'data-confirm' => 'Вы уверены?']);
                    },
                ],
            ],
        ],
    ]); ?>
</div>