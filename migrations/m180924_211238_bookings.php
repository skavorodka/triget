<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180924_211238_bookings
 */
class m180924_211238_bookings extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%bookings}}', [
            'id'                => Schema::TYPE_PK,
            'room_id'           => Schema::TYPE_INTEGER . ' NOT NULL',
            'name'              => Schema::TYPE_STRING . ' NOT NULL',
            'phone'             => Schema::TYPE_STRING . ' NOT NULL',
            'date'              => Schema::TYPE_DATE . ' NOT NULL',
            'status'            => Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 1',
            'created_at'        => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at'        => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%bookings}}');
    }
}
